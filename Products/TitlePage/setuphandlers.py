# -*- coding: utf-8 -*-
#
# File: setuphandlers.py
#
# Copyright (c) 2009 by McMaster University
# Generator: ArchGenXML Version 2.3
#            http://plone.org/products/archgenxml
#
# GNU General Public License (GPL)
#

__author__ = """Servilio Afre Puentes <afrepues@mcmaster.ca>"""
__docformat__ = 'plaintext'


import logging
logger = logging.getLogger('TitlePage: setuphandlers')
from Products.TitlePage.config import PROJECTNAME
from Products.TitlePage.config import DEPENDENCIES
import os
from Products.CMFCore.utils import getToolByName
import transaction
##code-section HEAD
##/code-section HEAD

def isNotTitlePageProfile(context):
    return context.readDataFile("TitlePage_marker.txt") is None



def updateRoleMappings(context):
    """after workflow changed update the roles mapping. this is like pressing
    the button 'Update Security Setting' and portal_workflow"""
    if isNotTitlePageProfile(context): return 
    wft = getToolByName(context.getSite(), 'portal_workflow')
    wft.updateRoleMappings()

def postInstall(context):
    """Called as at the end of the setup process. """
    # the right place for your custom code
    if isNotTitlePageProfile(context): return
    site = context.getSite()



##code-section FOOT
##/code-section FOOT
