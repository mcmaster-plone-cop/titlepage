# -*- coding: utf-8 -*-
#
# File: TitlePage.py
#
# Copyright (c) 2009 by McMaster University
# Generator: ArchGenXML Version 2.3
#            http://plone.org/products/archgenxml
#
# GNU General Public License (GPL)
#

__author__ = """Servilio Afre Puentes <afrepues@mcmaster.ca>"""
__docformat__ = 'plaintext'

from AccessControl import ClassSecurityInfo
from Products.Archetypes.atapi import *
from zope.interface import implements
import interfaces
from Products.ATContentTypes.atct import ATDocument
from Products.CMFDynamicViewFTI.browserdefault import BrowserDefaultMixin

from Products.TitlePage.config import *

# additional imports from tagged value 'import'
from Products.ATReferenceBrowserWidget.ATReferenceBrowserWidget import ReferenceBrowserWidget

##code-section module-header #fill in your manual code here
from random import choice
import transaction
from Products.CMFCore.utils import getToolByName
##/code-section module-header

schema = Schema((

    ReferenceField(
        name='tools',
        widget=ReferenceBrowserWidget(
            label='Tools',
            label_msgid='TitlePage_label_tools',
            i18n_domain='TitlePage',
        ),
        multiValued=True,
        relationship="featuredTool",
    ),

),
)

##code-section after-local-schema #fill in your manual code here
##/code-section after-local-schema

TitlePage_schema = OrderedBaseFolderSchema.copy() + \
    getattr(ATDocument, 'schema', Schema(())).copy() + \
    schema.copy()

##code-section after-schema #fill in your manual code here
TitlePage_schema['relatedItems'].widget.force_close_on_insert = False
##/code-section after-schema

class TitlePage(OrderedBaseFolder, ATDocument, BrowserDefaultMixin):
    """A title page for site roots and sections
    """
    security = ClassSecurityInfo()

    implements(interfaces.ITitlePage)

    meta_type = 'TitlePage'
    _at_rename_after_creation = True

    schema = TitlePage_schema

    ##code-section class-header #fill in your manual code here
    ##/code-section class-header

    # Methods

    security.declarePrivate('at_post_create_script')
    def at_post_create_script(self):
        """Hook to set itself as the default page.
        """
        transaction.savepoint(optimistic=True)
        self.saveDefaultPage(self.id)

    security.declarePublic('randomBannerImage')
    def randomBannerImage(self):
        """Returns a random image from the ones attached.
        """
        portal_membership = getToolByName(self, "portal_membership")
        user = portal_membership.getAuthenticatedMember()
        images = self.getFolderContents({'portal_type':'Image',
                                         'allowedRolesAndUsers': tuple(user.getRoles()) + ('user:%s' % user.getUserName(),), })

        if len(images):
            return choice(images)
        else:
            return None

    security.declarePublic('computeTools')
    def computeTools(self):
        """Returns the published tools.
        """
        uidcat = getToolByName(self, "uid_catalog")
        pcat = getToolByName(self, "portal_catalog")
	portal_path = getToolByName(self, "portal_url").getPortalPath()
        paths = ['%s/%s' % (portal_path,
                            ubrains[0].getPath())
                 for ubrains in map( lambda tid: uidcat(UID=tid),
                                     self["tools"] )
                 if len(ubrains)
                 ]
        brains = pcat(path = { 'query': paths,
                               'depth': 0,
                               },
                      review_state = 'published',
                      sort_by = 'getObjPositionInParent'
                      )[:]

        def comparePositions(b1, b2):
            b1pos = pcat.getIndexDataForRID(b1.getRID())["getObjPositionInParent"]
            b2pos = pcat.getIndexDataForRID(b2.getRID())["getObjPositionInParent"]
            return b1pos.__cmp__(b2pos)

        brains.sort( cmp = comparePositions )

        return brains


registerType(TitlePage, PROJECTNAME)
# end of class TitlePage

##code-section module-footer #fill in your manual code here
##/code-section module-footer



