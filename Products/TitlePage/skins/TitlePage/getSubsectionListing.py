## Script (Python) "getSubsectionListing"
##bind context=context
##bind namespace=
##parameters=section,contentFilter
##title=Get the objects to show in the current subsection
""" This method returns the objects to show in a subsection, returning
a dictionary with the results.
"""

from Products.CMFCore.utils import getToolByName
from Products.CMFPlone.utils import log

pcat = getToolByName(context, 'portal_catalog')

def getSeries(brain):
    """
    Determines if the object referenced by the catalog brains belongs
    to a series returning the one it belongs or None.

    For that, it walks the ancestors with the catalog inspecting the
    keywords for each one of them.
    """
    
    portal_url = getToolByName(context, 'portal_url')
    portal_path = portal_url.getPortalPath()
    path = '/'.join((pcat.getIndexDataForRID(brain.getRID())['path']).split('/')[:-1])
    # log('Starting with portal path "%s" and path "%s"' % (portal_path, path))
    while 0 < len(path) and path != '/' and path != portal_path:
        # Assuming ExtendedPathIndex here
        brains = pcat(path={'query': path, 'depth': 0})
        if 0 < len(brains):
            brain = brains[0]
            # log('Object\'s subject "%s"' % (brain.Subject,))
            if 'Series' in brain.Subject:
                return brain
        path = '/'.join(path.split('/')[:-1])
        # log('Next path: "%s"' % (path,))
    return None

try:
    # If section is a result from a catalog query, get the object
    section = section.getObject()
except:
    pass

# For Alias content type, get the aliased object
try:
    section = section.getAlias()
except:
    pass

if section.portal_type in ('Topic', 'RichTopic'):
    brains = section.queryCatalog()
else:
    brains = section.getFolderContents(contentFilter=contentFilter)

listing = {}

for brain in brains:
    # log('Calculating series for %s...' % (brain,))
    series = getSeries(brain)
    if series is None:
        listing[brain.getRID()] = brain
    else:
        listing[series.getRID()] = series

return listing.values()
