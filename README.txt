Description

    This product implements a document type to be used as default page
    in a site's root folder and sections.

Installation

    Place TitlePage in the Products directory of your Zope instance
    and restart the server.

    Go to the 'Site Setup' page in the Plone interface and click on
    the 'Add/Remove Products' link.

    Choose TitlePage (check its checkbox) and click the 'Install'
    button. You should see the change immediately.

    Now you should be able to add a title page to any folder in your
    website.

    Uninstall -- This can be done from the same management screen. All
    content created is kept, so folder with a title page will show an
    error when accessed.

Written by

    Servilio Afre Puentes <afrepues@mcmaster.ca>
