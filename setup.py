from setuptools import setup, find_packages
import os

version = '1.2'

setup(name='Products.TitlePage',
      version=version,
      description="Page with a header image, good for homepage and sections.",
      long_description=open("README.txt").read() + "\n" +
                       open(os.path.join("docs", "HISTORY.txt")).read(),
      # Get more strings from
      # http://pypi.python.org/pypi?%3Aaction=list_classifiers
      classifiers=[
        "Framework :: Plone",
        "Programming Language :: Python",
	"License :: DFSG approved",
	"Development Status :: 6 - Mature",
        ],
      keywords='',
      author='Servilio Afre Puentes',
      author_email='afrepues@mcmaster.ca',
      url='http://plone.mcmaster.ca/bzr/afrepues/TitlePage',
      license='GPL',
      packages=find_packages(exclude=['ez_setup']),
      namespace_packages=['Products'],
      include_package_data=True,
      zip_safe=False,
      install_requires=[
          'setuptools',
          # -*- Extra requirements: -*-
      ],
      entry_points="""
      # -*- Entry points: -*-

      [z3c.autoinclude.plugin]
      target = plone
      """,
      )
